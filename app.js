import express from "express";
import fs from "fs";
import axios from "axios";
import cors from "cors";

import { convertToFormData } from "./utils.js";
import requestParser from "./formidable.js";
import fileUpload from "./middleware/fileUpload.js";

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static("public"));
app.use(
  cors({
    origin: "*",
  })
);

app.set("view engine", "ejs");
app.set("views", "./views");

app.get("/shopify/auth", (req, res) => {
  const authToken = req.headers.authorization;
  if (!authToken) {
    return res.status(401).json({ error: "Auth Token is not present" });
  }
  fs.writeFileSync("./token/auth.key", authToken, "utf8");
  res.status(200).json({ message: "Success" });
});

app.get("/token", (req, res) => {
  try {
    const authToken = fs.readFileSync("./token/auth.key", "utf8");
    res.status(200).json({ token: authToken });
  } catch (err) {
    res.status(500).json({
      error: err.message,
      stack: err.stack,
      server: "error in test server",
      tip: "Make sure the testing shopify app is running perfectly",
    });
  }
});

app.post(
  "/test/shopify",
  fileUpload(requestParser).fileUploadMiddleWare,
  (req, res) => {
    try {
      let { url, method, headers, params, authentication } = req.body;
      if (authentication === "true" || authentication === true) {
        headers.authorization = fs.readFileSync("./token/auth.key", "utf8");
      }
      let formData = params;
      console.log(req.body)
      console.log(req.files)
      if (req.files?.file) {
        formData = convertToFormData(params);
        formData.append(
          "file",
          fs.createReadStream(req.files.file[0].filepath)
        );
        headers = {
          ...headers,
          ...formData.getHeaders(),
        };
      }
      const request = axios.create({
        baseURL: url,
        headers: {
          ...headers,
        },
      });
      let reqHeaders = {};
      request.interceptors.request.use((config) => {
        reqHeaders = config.headers;
        return config;
      });

      let axiosRequest;
      let startTime = Date.now();
      switch (method.toLowerCase()) {
        case "get":
          axiosRequest = request.get("", { params });
          break;
        case "post":
          axiosRequest = request.post("", formData, {
            headers: {
              ...headers,
            },
          });
          break;
        case "put":
          axiosRequest = request.put("", formData, {
            headers: {
              ...headers,
            },
          });
          break;
        case "delete":
          axiosRequest = request.delete("");
          break;
        case "patch":
          axiosRequest = request.patch("", params);
          break;
        default:
          return res.status(400).json({ error: "Invalid request method" });
      }

      axiosRequest
        .then((response) => {
          let responseTime = (Date.now() - startTime) / 1000;
          res.status(200).json({
            status: response.status || 200,
            reqHeaders: reqHeaders || {},
            resHeaders: response.headers || {},
            reqBody: params || {},
            resBody: response.data || {},
            responseTime,
          });
        })
        .catch((error) => {
          let responseTime = (Date.now() - startTime) / 1000;
          res.status(200).json({
            status:
              error.response?.data?.statusCode ||
              error.response?.status ||
              error.status ||
              500,
            reqHeaders: reqHeaders || {},
            resHeaders: error.response?.headers || {},
            reqBody: params || {},
            resBody: error.response?.data || { data: error.data } || {},
            responseTime,
          });
        });
    } catch (err) {
      req.file?.path &&
        fs.unlink(req.file.path, (err) => {
          if (err) console.error("Error deleting file:", err);
        });
      res.status(500).json({
        error: err.message,
        stack: err.stack,
        server: "error in test server",
        tip: "Make sure the testing shopify app is running perfectly",
      });
    }
  }
);

app.get("/", (req, res) => {
  res.render("index");
});

app.listen(3001, () => console.log("Server is running"));
