import formidable from "formidable";
export default function requestParser(req, callback) {
  const requestParser = formidable({
    keepExtensions: true,
    multiples: true,
  });
  requestParser.parse(req, (err, fields, files) => {
    if (err) {
      callback(err);
    } else {
      callback(null, fields, files);
    }
  });
}
