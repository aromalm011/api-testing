import formData from "form-data";
export const convertToFormData = (data) => {
  const FormData = new formData();
  for (const key in data) {
    if (Object.hasOwnProperty.call(data, key)) {
      const value = data[key];
      if (
        typeof value === "object" &&
        !(value instanceof File) &&
        !(value instanceof Blob)
      ) {
        FormData.append(key, JSON.stringify(value));
      } else {
        FormData.append(key, value);
      }
    }
  }
  return FormData;
};

export default function parseNestedObjects(obj) {
  for (let key in obj) {
    if (typeof obj[key] === "string") {
      try {
        obj[key] = JSON.parse(obj[key]);
        parseNestedObjects(obj[key]);
      } catch (err) {
        // Not a JSON string, do nothing
      }
    } else if (typeof obj[key] === "object") {
      parseNestedObjects(obj[key]);
    }
  }
  return obj;
}
