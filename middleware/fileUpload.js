import parseNestedObjects from "../utils.js";

export default function fileUpload(requestParser) {
  const fileUploadMiddleWare = (req, _res, next) => {
    if (!req.headers["content-type"]?.includes("multipart/form-data"))
      return next();
    requestParser(req, (err, fields, files) => {
      if (err) {
        return next(new Error(err));
      }
      req.files = files;
      Object.keys(fields).forEach((key) => {
        const fieldValue = fields[key];
        if (fieldValue.length === 1) {
          req.body[key] = fieldValue[0];
        } else {
          req.body[key] = fieldValue;
        }
      });
      parseNestedObjects(req.body);
      next();
    });
  };
  return Object.freeze({
    fileUploadMiddleWare,
  });
}
